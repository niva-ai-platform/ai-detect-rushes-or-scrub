from detect_rushes_or_scrub import detect_rushes_or_scrub
from ai_python_detect_wrapper_library import AiDetectWrapper

ai_detect_wrapper = AiDetectWrapper()
ai_detect_wrapper.register_service()
ai_detect_wrapper.create_download_dir()
ai_detect_wrapper.initialise_app(detect_rushes_or_scrub)

app = ai_detect_wrapper.get_app()
