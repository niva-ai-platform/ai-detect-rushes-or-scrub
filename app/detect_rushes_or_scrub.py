import tensorflow as tf
from tensorflow import keras


model = keras.models.load_model("./model/ai-detect-rushes-or-scrub-2022-11-08.h5")


def detect_rushes_or_scrub(file_in, confidence_threshold=0.5):
    image = keras.preprocessing.image.load_img(
        file_in, target_size=(200, 200)
    )
    image_array = keras.preprocessing.image.img_to_array(image)
    image_reshaped = tf.expand_dims(image_array, 0)  # Create batch axis

    prediction = model.predict(image_reshaped)
    confidence = float(prediction[0][0])

    tags = []
    if confidence <= 1 - confidence_threshold:
        tags.append('rushes_detected')
    elif confidence >= confidence_threshold:
        tags.append('scrub_detected')

    output_data = {
        'tags': tags,
        'results': {
            'targetDetected': bool(tags),
            'maxConfidence': confidence
        }
    }
    return output_data
